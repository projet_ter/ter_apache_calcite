package org.apache.calcite.rel.rules;
import org.apache.calcite.plan.RelOptRule;
import org.apache.calcite.plan.RelOptRuleCall;
import org.apache.calcite.plan.RelOptRuleOperand;
import org.apache.calcite.plan.RelOptUtil;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rel.core.EquiJoin;
import org.apache.calcite.rel.core.Filter;
import org.apache.calcite.rel.core.Join;
import org.apache.calcite.rel.core.JoinRelType;
import org.apache.calcite.rel.core.RelFactories;
import org.apache.calcite.rel.type.RelDataType;
import org.apache.calcite.rex.RexBuilder;
import org.apache.calcite.rex.RexNode;
import org.apache.calcite.rex.RexUtil;
import org.apache.calcite.tools.RelBuilder;
import org.apache.calcite.rel.core.TableScan;
import org.apache.calcite.rel.logical.LogicalFilter;
import org.apache.calcite.rel.logical.LogicalTableScan;
import org.apache.calcite.tools.RelBuilderFactory;
import org.apache.calcite.rel.RelNode;
import org.apache.calcite.rex.RexCall;
import org.apache.calcite.rel.core.RelFactories;
import org.apache.calcite.rel.rules.FilterTableScanRule;
import org.apache.calcite.runtime.PredicateImpl;
import org.apache.calcite.schema.FilterableTable;
import org.apache.calcite.schema.ProjectableFilterableTable;
import org.apache.calcite.util.ImmutableIntList;
import org.apache.calcite.adapter.enumerable.EnumerableInterpreter;
import org.apache.calcite.interpreter.Bindables;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import org.apache.calcite.util.mapping.Mapping;
import org.apache.calcite.util.mapping.Mappings;

public class ScanFilterRule extends RelOptRule {

 public ScanFilterRule(RelOptRuleOperand operand, String description) {
   super(operand, "Scan_filter_rule:" + description);
 }

 public static final ScanFilterRule INSTANCE =
     new ScanFilterRule(
         operand(
             Filter.class,
             operand(TableScan.class, none())),
         "filter_tableScan");

         @Override public void onMatch(RelOptRuleCall call) {
          // LogicalFilter filter = (LogicalFilter) call.rels[0];
          // TableScan tableScan = (TableScan) call.rels[1];
          final Filter filter = call.rel(0);
          final TableScan tableScan = call.rel(1);

           	RexNode nodeCond = filter.getCondition();
           //	RexNode nodeOperand = ((RexCall) nodeCond).getOperands();
           	boolean etat= false;
           	switch (nodeCond.getKind()) {
           		 case GREATER_THAN:
           			etat = true;
           			break;
           		 case LESS_THAN:
           			etat= false;
           			break;
           		 default:
           			etat = false;
           	}
           	if(etat ){
              call.transformTo(tableScan);
           	}
           	else{
           		apply(call, filter, tableScan);
           	}
}
 protected void apply(RelOptRuleCall call, Filter filter, TableScan scan) {
    final ImmutableIntList projects;
    final ImmutableList.Builder<RexNode> filters = ImmutableList.builder();
    if (scan instanceof Bindables.BindableTableScan) {
      final Bindables.BindableTableScan bindableScan =
          (Bindables.BindableTableScan) scan;
      filters.addAll(bindableScan.filters);
      projects = bindableScan.projects;
    } else {
      projects = scan.identity();
    }

    final Mapping mapping = Mappings.target(projects,
        scan.getTable().getRowType().getFieldCount());
    filters.add(
        RexUtil.apply(mapping, filter.getCondition()));

    call.transformTo(
        Bindables.BindableTableScan.create(scan.getCluster(), scan.getTable(),
            filters.build(), projects));
  }
}